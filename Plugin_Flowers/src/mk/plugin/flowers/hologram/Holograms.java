package mk.plugin.flowers.hologram;

import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_12_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import com.google.common.collect.Maps;

import mk.plugin.flowers.flower.Flower;
import mk.plugin.flowers.flower.Flowers;
import net.minecraft.server.v1_12_R1.EntityArmorStand;
import net.minecraft.server.v1_12_R1.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_12_R1.PacketPlayOutSpawnEntityLiving;
import net.minecraft.server.v1_12_R1.WorldServer;

public class Holograms {
	
	private static Map<String, EntityArmorStand> livingHolograms = Maps.newHashMap();
	
	public static void sendPacket(Player player, String id, boolean overwrite) {
		if (!overwrite) if (livingHolograms.containsKey(id)) return;
		sendPacket(player, id);
	}
	
	public static void sendPacket(Player player, String id) {
		Flower flower = Flowers.get(id);
		String name = Flowers.getItemStackName(flower.getItemStack());
		
		EntityArmorStand stand = null;
		if (livingHolograms.containsKey(id)) {
			stand = livingHolograms.get(id);
		} else {
			WorldServer world = ((CraftWorld) flower.getLocation().getWorld()).getHandle();
			stand = new EntityArmorStand(world);
	        
	        stand.setLocation(flower.getLocation().getX() + 0.5, flower.getLocation().getY() - 0.6, flower.getLocation().getZ() + 0.5, 0, 0);
	        stand.setCustomName(name);
	        stand.setCustomNameVisible(true);
	        stand.setNoGravity(true);
	        stand.setInvisible(true);
	        
	        livingHolograms.put(id, stand);
		}
        
        PacketPlayOutSpawnEntityLiving packet = new PacketPlayOutSpawnEntityLiving(stand);
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
	}
	
	public static void remove(String id) {
		if (!livingHolograms.containsKey(id)) return;
		EntityArmorStand stand = livingHolograms.get(id);
		Bukkit.getOnlinePlayers().forEach(player -> {
			PacketPlayOutEntityDestroy packet = new PacketPlayOutEntityDestroy(stand.getId());
			((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
		});
		livingHolograms.remove(id);
	}
	
}

package mk.plugin.flowers.command;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Sets;

import mk.plugin.flowers.flower.Flowers;
import mk.plugin.flowers.main.MainFlowers;
import mk.plugin.flowers.yamlfile.YamlFile;

public class FlowerCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		
		if (args.length == 0) {
			sendTut(sender);
			return false;
		}
		
		if (args[0].equalsIgnoreCase("reload")) {
			MainFlowers.get().reloadConfigs();
			sender.sendMessage("§aReloaded");
		}
		
		else if (args[0].equalsIgnoreCase("putdown")) {
			Player player = (Player) sender;
			String id = args[1];
			ItemStack is = player.getInventory().getItemInMainHand();
			Location lc = player.getTargetBlock(Sets.newHashSet(Material.AIR), 10).getLocation().add(0, 1, 0);
			Flowers.save(MainFlowers.get(), YamlFile.CONFIG, is, id, lc);
			Flowers.spawn(id);
			
			player.sendMessage("§aSaved, wait for task summon block");
		}
		
		
		return false;
	}
	
	public void sendTut(CommandSender sender) {
		sender.sendMessage("");
		sender.sendMessage("§6§lFlowers by MankaiStep - KeoDeoVN");
		sender.sendMessage("/flowers reload: Reload configs");
		sender.sendMessage("/flowers putdown <id>: Save your flower in your hand");
		sender.sendMessage("");
	}

}

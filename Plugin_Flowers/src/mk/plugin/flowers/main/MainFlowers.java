package mk.plugin.flowers.main;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import mk.plugin.flowers.command.FlowerCommand;
import mk.plugin.flowers.flower.Flowers;
import mk.plugin.flowers.hologram.Holograms;
import mk.plugin.flowers.lang.Lang;
import mk.plugin.flowers.listener.FlowerListener;
import mk.plugin.flowers.yamlfile.YamlFile;

public class MainFlowers extends JavaPlugin {
	
	@Override
	public void onEnable() {
		this.regsiterListeners();
		this.registerCommands();
		if (Bukkit.getOnlinePlayers().size() > 0) this.reloadConfigs();
	}
	
	public void reloadConfigs() {
		this.saveDefaultConfig();
		YamlFile.reloadAll(this);
		Lang.init(this, YamlFile.LANG);
		Flowers.loadAll(YamlFile.CONFIG);
		Flowers.flowers.keySet().forEach(flower -> {
			Bukkit.getOnlinePlayers().forEach(player -> {
				Holograms.sendPacket(player, flower);
			});
		});
	}
	
	public void regsiterListeners() {
		Bukkit.getPluginManager().registerEvents(new FlowerListener(), this);
	}
	
	public void registerCommands() {
		this.getCommand("flowers").setExecutor(new FlowerCommand());
	}
	
	public static MainFlowers get() {
		return MainFlowers.getPlugin(MainFlowers.class);
	}
	
}

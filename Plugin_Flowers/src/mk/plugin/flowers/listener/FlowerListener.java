package mk.plugin.flowers.listener;

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;

import mk.plugin.flowers.flower.Flowers;
import mk.plugin.flowers.hologram.Holograms;
import mk.plugin.flowers.lang.Lang;
import mk.plugin.flowers.main.MainFlowers;
import mk.plugin.flowers.yamlfile.YamlFile;

public class FlowerListener implements Listener {
	
	private boolean join = false;
	
	@EventHandler
	public void onMove(PlayerMoveEvent e) {
		Player player = e.getPlayer();
		Flowers.flowers.forEach((id, flower) -> {
			Location fl = flower.getLocation();
			if (player.getWorld() == fl.getWorld()) {
				if (fl.distance(player.getLocation()) < 25) {
					if (player.hasMetadata("flower." + id)) return;
					player.setMetadata("flower." + id, new FixedMetadataValue(MainFlowers.get(), ""));
					Holograms.sendPacket(player, id);
					return;
				}
			}
			if (player.hasMetadata("flower." + id)) player.removeMetadata("flower." + id, MainFlowers.get());;
		});
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		if (!join) {
			join = true;
			MainFlowers.get().reloadConfigs();
			System.out.println("[Flowers] Reloaded configs");
		}
	}
	
//	@EventHandler
//	public void onTeleport(PlayerTeleportEvent e) {
//		if (e.getFrom().getWorld() == e.getTo().getWorld()) return;
//		Bukkit.getScheduler().runTaskAsynchronously(MainFlowers.get(), () -> {
//			Player player = e.getPlayer();
//			Flowers.flowers.keySet().forEach(flower -> {
//				Holograms.sendPacket(player, flower);
//			});
//			System.out.println("[Flowers] Sent flowers' packets to " + player.getName());
//		});
//	}
	
	@EventHandler
	public void onBreak(BlockBreakEvent e) {
		Block block = e.getBlock();
		if (!Flowers.isFlower(block)) return;
		Player player = e.getPlayer();
		if (!player.hasPermission("flower.break"))  {
			e.setCancelled(true);
			return;
		}
		if (player.isSneaking()) {
			Flowers.remove(MainFlowers.get(), YamlFile.CONFIG, block);
			player.sendMessage("§cRemoved");
		}
		else {
			e.setCancelled(true);
			player.sendMessage("§cShift + Break to remove");
		}
	}
	
	@EventHandler
	public void onSneak(PlayerToggleSneakEvent e) {
		if (!e.isSneaking()) return;
		Player player = e.getPlayer();
		
		String id = Flowers.getNearest(player.getLocation(), 2);
		if (id == null) return;

		Location location = Flowers.get(id).getLocation();
		
		new BukkitRunnable() {
			
			int i = 0;
			final int seconds = 5;
			final int count = seconds * 4;
			
			@Override
			public void run() {
				i++;
				
				if (!player.isSneaking()) {
					this.cancel();
					return;
				}
				
				if (player.getLocation().distance(location) > 2) {
					this.cancel();
					return;
				} 
				
				if (i >= count) {
					this.cancel();
					
					// Give
					ItemStack result = Flowers.get(id).getItemStack().clone();
					result.setAmount(1);
					player.getInventory().addItem(result);
					Lang.PLUCKED_MESSAGE.send(player, "%amount%", result.getAmount() + "", "%item_name%", Flowers.getItemStackName(result));
					player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1, 1);
					return;
				}
				
				String s = "";
				for (int j = 0 ; j < i * 2 ; j++) {
					s += Lang.PLUCKING_SUBTITLE_COLOR_1 + Lang.PLUCKING_SUBTITLE_ICON.get();
				}
				for (int j = i * 2; j < count * 2; j++) {
					s += Lang.PLUCKING_SUBTITLE_COLOR_2 + Lang.PLUCKING_SUBTITLE_ICON.get();
				}
				
				player.sendTitle(Lang.PLUCKING_TITLE.get().replace("%item_name%", Flowers.getItemStackName(Flowers.get(id).getItemStack())), s, 0, 10, 0);
				player.playSound(player.getLocation(), Sound.BLOCK_GRAVEL_PLACE, 1, 1);
			}
		}.runTaskTimerAsynchronously(MainFlowers.get(), 0, 5);
	}
	
}

package mk.plugin.flowers.lang;

import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.collect.Maps;

import mk.plugin.flowers.yamlfile.YamlFile;

public enum Lang {
	
	PLUCKING_TITLE("%item_name%"),
	PLUCKING_SUBTITLE_COLOR_1("&a"),
	PLUCKING_SUBTITLE_COLOR_2("&7"),
	PLUCKING_SUBTITLE_ICON("|"),
	PLUCKED_MESSAGE("You have gotten x%amount% %item_name%");
	
	private String value;
	
	private Lang(String value) {
		this.value = value;
	}
	
	public String get() {
		return this.value.replace("&", "§");
	}
	
	public void set(String value) {
		this.value = value;
	}
	
	public String getName() {
		return this.name().toLowerCase().replace("_", "-").replace("&", "§");
	}
	
	@Override
	public String toString() {
		return this.value.replace("&", "§");
	}
	
	public void send(Player player, Map<String, String> placeholders) {
		String s = this.get();
		for (Entry<String, String> e : placeholders.entrySet()) {
			s = s.replace(e.getKey(), e.getValue());
		}
		player.sendMessage(s);
	}
	
	public void send(Player player) {
		String s = this.get();
		player.sendMessage(s);
	}
	
	public void send(Player player, String key1, String value1, String key2, String value2) {
		Map<String, String> placeholders = Maps.newHashMap();
		placeholders.put(key1, value1);
		placeholders.put(key2, value2);
		send(player, placeholders);
	}
	
	public static void init(JavaPlugin plugin, YamlFile lang) {
		FileConfiguration config = lang.get();
		for (Lang l : Lang.values()) {
			if (config.contains(l.getName())) {
				l.set(config.getString(l.getName()).replace("&", "§"));
			}
			else {
				config.set(l.getName(), l.get().replace("§", "&"));
				lang.save(plugin);
			}
		}
	}
	
}

package mk.plugin.flowers.flower;

import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import mk.plugin.flowers.hologram.Holograms;
import mk.plugin.flowers.yamlfile.YamlFile;

public class Flowers {
	
	public static Map<String, Flower> flowers = Maps.newHashMap();
	public static Map<String, Hologram> holograms = Maps.newHashMap();
	
	public static void save(Plugin plugin, YamlFile yamlFile, ItemStack is, String id, Location lc) {
		flowers.put(id, new Flower(is, lc));
		saveAll(plugin, yamlFile);
	}
	
	public static void saveAll(Plugin plugin, YamlFile yamlFile) {
		FileConfiguration config = yamlFile.get();
		flowers.forEach((id, flower) -> {
			config.set("flower." + id + ".item", flower.getItemStack().serialize());
			config.set("flower." + id + ".location", flower.getLocation().serialize());
		});
		yamlFile.save(plugin);
	}
	
	public static void loadAll(YamlFile yamlFile) {
		FileConfiguration config = yamlFile.get();
		flowers.clear();
		config.getConfigurationSection("flower").getKeys(false).forEach(id -> {
			ItemStack is = ItemStack.deserialize(config.getConfigurationSection("flower." + id + ".item").getValues(false));
			Location lc = Location.deserialize(config.getConfigurationSection("flower." + id + ".location").getValues(false));
			flowers.put(id, new Flower(is, lc));
		});
	}
	
	public static boolean isFlower(Block block) {
		for (Flower flower : flowers.values()) {
			if (flower.getLocation().getWorld() == block.getWorld() && flower.getLocation().distance(block.getLocation()) < 0.5) return true;
		}
		return false;
	}
	
	public static void remove(Plugin plugin, YamlFile yamlFile, Block block) {
		Lists.newArrayList(flowers.keySet()).forEach(id -> {
			Flower f = get(id);
			if (f.getLocation().getWorld() == block.getWorld() && f.getLocation().distance(block.getLocation()) < 0.5) {
				flowers.remove(id);
				saveAll(plugin, yamlFile);
				Holograms.remove(id);
				return;
			}
		});
	}
	
	public static Flower get(String id) {
		return flowers.getOrDefault(id, null);
	}
	
	public static String getNearest(Location lc, double radius) {
		for (String id : Lists.newArrayList(flowers.keySet())) {
			Flower f = get(id);
			if (f.getLocation().getWorld() == lc.getWorld()) {
				if (f.getLocation().distance(lc) <= radius) return id;
			}
		}
		
		return null;
	}

	public static void spawn(String id) {
		Flower flower = get(id);
		flower.getLocation().getBlock().setType(flower.getItemStack().getType());
		Bukkit.getOnlinePlayers().forEach(player -> {
			Holograms.sendPacket(player, id);
		});
	}
	
	
	
	public static String getItemStackName(ItemStack is) {
		if (is.hasItemMeta()) {
			if (is.getItemMeta().hasDisplayName()) return is.getItemMeta().getDisplayName();
		}
		return is.getType().name();
	}
	
	public static Location calculateHologramLocation(Location fl) {
		return fl.clone().add(0, 1.2, 0);
	}
	
}

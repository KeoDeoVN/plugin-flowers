package mk.plugin.flowers.flower;

import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;

public class Flower {
	
	private ItemStack itemStack;
	private Location location;
	
	public Flower(ItemStack itemStack, Location location) {
		this.itemStack = itemStack;
		this.location = location;
	}
	
	public ItemStack getItemStack() {
		return this.itemStack;
	}
	
	public Location getLocation() {
		return this.location;
	}

}
